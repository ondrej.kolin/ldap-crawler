from gi.repository import Gtk, GObject, Gio
from ..utils.connection import Connection
from ..utils.schema import schema



class HistoryView(Gtk.TreeView):
    __gsignals__ = {
        'entry-selected': (GObject.SIGNAL_ACTION, None, (Connection, ))
    }

    store = Gtk.TreeStore(str, str, Connection)

    def __init__(self):
        super().__init__(headers_visible = False)
        self.connect("row-activated", self.row_activated)
        self.set_model(self.store)

        self._setup_widget()
        self._load_data()


    def _setup_widget(self):
        uri_renderer = Gtk.CellRendererText()
        binddn_renderer = Gtk.CellRendererText()

        column = Gtk.TreeViewColumn()
        column.pack_start(uri_renderer, True)
        column.pack_start(binddn_renderer, True)

        column.add_attribute(uri_renderer, "markup", 0)
        column.add_attribute(binddn_renderer, "markup", 1)

        column.get_area().set_orientation(Gtk.Orientation.VERTICAL)

        self.append_column(column)
        self.show_all()

    def clear(self):
        for row in self.store:
            self.store.remove(row.iter)

    def _load_data(self):
        # Solve reloading schema. One list for all data
        self.clear()
        for data in schema['history']:
            print("HISTORY: Found {}".format(data))
            self.prepend_connection(Connection.from_json(data))

    def add_to_history(self, connection):
        print("HISTORY: Storing {}".format(connection))
        connection_json = connection.to_json()
        history = schema['history']

        if connection_json in history:
            history.remove(connection_json)

        history.append(connection_json)
        schema['history'] = history
        self._load_data()


    def prepend_connection(self, connection):
        self.store.prepend(None, (connection.host + "/" + connection.base_dn, "bind_dn placeholder", connection))


    def append_connection(self, connection):
        self.store.append(None, (connection.host + "/" + connection.base_dn, "bind_dn placeholder", connection))


    def row_activated(self, treeview, path, col):
        iter = self.store.get_iter(path)
        self.emit("entry-selected", self.store.get_value(iter, 2))

    
