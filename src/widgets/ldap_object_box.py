from gi.repository import Gtk



class LdapObjectBox(Gtk.Grid):
    ATTR_COL = 0
    VALUE_COL = 1

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_halign(Gtk.Align.FILL)

    def load_data(self, dn, data):
        self.clear()
        # TODO Display fancy message on empty DN
        if data is None:
            return False
        self.dn = dn
        dn_label = Gtk.Label(dn)
        dn_label.set_selectable(True)
        self.attach(dn_label, self.ATTR_COL, 0, 1, 1)
        row = 1
        for attr, values in data.items():
            attr_label = Gtk.Label(attr)
            attr_label.set_selectable(True)
            self.attach(attr_label, self.ATTR_COL, row, 1, 1)
            for value in values:
                value = value.decode()
                value_entry = Gtk.Entry()
                value_entry.set_hexpand(True)
                value_entry.set_text(value)
                self.attach(value_entry, self.VALUE_COL, row, 1, 1)
                row += 1
        self.show_all()

    def clear(self):
        for child in self.get_children():
            self.remove(child)


        
