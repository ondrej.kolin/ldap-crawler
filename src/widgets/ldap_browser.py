from gi.repository import Gtk, GObject
from gi.repository import GdkPixbuf
import ldap
from ..utils.browserpopulator import BrowserPopulator

class LdapBrowser(Gtk.TreeView):
    ICON_SIZE = 18
    __gsignals__ = {
        'entry-selected': (GObject.SIGNAL_ACTION, None, (str,))
    }
                        # name, dn_part, icon, complet dn
    store = Gtk.TreeStore(str, str, GdkPixbuf.Pixbuf, str)
    data_store = {}
    connection = None
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_model(self.store)
        kwargs['headers_visible'] = False
        # Configure renderers
        self._set_renderers()
        self.connect("row-activated", self.entry_selected)
        # self.connect("button-press-event", self.on_button_press_event)

    def _set_renderers(self):
        column = Gtk.TreeViewColumn("")
        text = Gtk.CellRendererText()
        icon = Gtk.CellRendererPixbuf()
        column.pack_start(icon, False)
        column.pack_start(text, True)
        column.add_attribute(icon, "pixbuf", 2)
        column.add_attribute(text, "text", 0)
        self.append_column(column)
        self.set_tooltip_column(1)

    def dn_data(self, dn):
        try:
            return self.data_store[dn][1]
        except KeyError:
            return None

    def clear(self):
        self.store = Gtk.TreeStore(str, str, GdkPixbuf.Pixbuf, str)
        self.set_model(self.store)
        self.data_store = {}
        #for row in self.store:
        #    self.store.remove(row.iter)

    def load_data(self, connection):
        self.connection = connection
        populator = BrowserPopulator(self)
        populator.connection = connection
        populator.start()


    def entry_selected(self, obj, path, column):
        dn = self.store.get_value(self.store.get_iter(path), 3)
        self.emit("entry-selected", dn)







