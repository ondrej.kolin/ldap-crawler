# window.py
#
# Copyright 2019 Ondrej Kolin
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, GObject
from .widgets.ldap_browser import LdapBrowser
from .widgets.history_view import HistoryView
from .widgets.ldap_object_box import LdapObjectBox
from .dialogs.connection import ConnectionDialog
from .utils.connection import Connection
import ldap


@Gtk.Template(resource_path='/cz/ondrejkolin/ldapcrawler/window.ui')
class LdapcrawlerWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'LdapcrawlerWindow'

    # Custom Widgets
    crawler = LdapBrowser()
    object_box = LdapObjectBox()
    history = HistoryView()

    # Childs from template
    content = Gtk.Template.Child()
    displayer = Gtk.Template.Child()
    history_scroll_window = Gtk.Template.Child()
    history_popover = Gtk.Template.Child()
    header_bar = Gtk.Template.Child()
    add_connection = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        # Widgets
        self.content.add(self.crawler)
        self.displayer.add(self.object_box)
        self.history_scroll_window.add(self.history)
        self.show_all()

        # Signals
        self.crawler.connect("entry-selected", self.display_ldap)
        self.add_connection.connect("clicked", self.new_connection)
        self.history.connect("entry-selected", self.history_callback)

    def connect(self, connection):
        print("Connecting to:", connection.host, connection.base_dn)
        try:
            self.crawler.load_data(connection)
            print("Connected")
        except ldap.INSUFFICIENT_ACCESS:
            print("Not enough rights")
            return False
        except ldap.NO_SUCH_OBJECT:
            print("Not found!")
            return False
        except ldap.SERVER_DOWN:
            print("Did not found LDAP server")
            return False
        self.history.add_to_history(connection)




    def display_ldap(self, src_object, dn):
        self.header_bar.set_subtitle(self.crawler.connection.host + "/" + dn)
        self.object_box.load_data(dn, self.crawler.dn_data(dn))

    def new_connection(self, src_obj):
        connection_dialog = ConnectionDialog(self)
        if connection_dialog.run() == Gtk.ResponseType.OK:
            print("Connection triggered")
            self.connect(connection_dialog.get_connection())
        else:
            print("Not ok")
        connection_dialog.hide()


    def history_callback(self, who, connection):
        self.history_popover.popdown()
        self.connect(connection)
        pass





        
