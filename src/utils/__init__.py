from gi.repository import Gtk

def pixbuf_from_ldap(dn, data=None, cache={}, icon_size=18):
    icon_name = "text-x-generic"
    if dn[:2] == "dc" or dn[:2] == "ou":
        icon_name = "folder-symbolic"
    if data is not None:
        if 'objectClass' in data and (b'posixGroup' in data['objectClass']  or b'groupOfNames' in data['objectClass']):
            icon_name = 'system-users-symbolic'

    icon = Gtk.IconTheme.get_default().lookup_icon(icon_name, icon_size, Gtk.IconLookupFlags.FORCE_SYMBOLIC)
    return cache.setdefault(icon_name, icon.load_icon())
