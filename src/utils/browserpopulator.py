import threading
from . import pixbuf_from_ldap
import socket
import ldap

class BrowserPopulator(threading.Thread):

    error = None
    browser = None
    _socket = None
    _ldap_connection = None


    def __init__(self, browser):
        super(BrowserPopulator, self).__init__()
        self.browser = browser
        self.connection = None

    def run(self):
        self.browser.clear()
        print("CRAWLER: Online, ready to work")
        # HELPER FUNCTIONS
        # Read one leven of the ldap data
        def crawl_ldap(base_dn, parent):
            print(f"CRAWLER: Diving into {base_dn}")
            l.search(base_dn, ldap.SCOPE_ONELEVEL)
            res, data = l.result()
            for dn, dn_data in data:
                piter = self.browser.store.append(parent, [
                                                dn.split(",")[0].split("=")[1],
                                                dn.split(",")[0],
                                                pixbuf_from_ldap(dn, dn_data),
                                                dn
                                            ])
                self.browser.data_store[dn] = (piter, dn_data)
                crawl_ldap(dn, piter)
        # END OF HELPER FUNCTIONS
        if self.connection is None:
            raise AttributeError("No connection defined")

        # TODO Rework loading to use only one ldap request and don't use recursion
        # Test the connection
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            print(f"CRAWLER: Testing the connection {self.connection.hostname}")
            self._socket.connect((self.connection.hostname, self.connection.port))
            self._socket.shutdown(socket.SHUT_RDWR)
        except (ConnectionResetError, TimeoutError) as e:
            # connection canceled, port can not be connected
            print("CRAWLER: TCP Connection to the server was unsuccesfull")
            self.error = e
            return
        except (socket.gaierror) as e:
            print("CRAWLER: Error in name resolution")
            self.error = e
            return
        # Open the connection and read base_dn if necessary
        l = ldap.initialize(self.connection.host)
        base_dn = self.connection.base_dn
        if len(base_dn) == 0:
            try:
                print("CRAWLER: Reading the base_dn")
                base_dn = l.get_naming_contexts()[0].decode()
            except IndexError as e:
                print("CRAWLER: No naming context found in the server")
                self.error = e
                return
            except ldap.SERVER_DOWN as e:
                print("CRAWLER: Server down")
                self.error = e
                return

        # Adding the base_dn to the store of the browser
        parent = None
        path = []
        if "=" in base_dn:
            print("CRAWLER: Browsing the base_dn {}".format(base_dn))
            parts = base_dn.split(",")
            parts.reverse()
            for index, part in enumerate(parts):
                path.append(part)
                print(f"CRAWLER: BASE_DN {index} {part}")
                parent = self.browser.store.append(parent,
                                                   (part.split("=")[1],
                                                    part,
                                                    pixbuf_from_ldap(part),
                                                    ",".join(path)))
                print("CRAWLER: BASE_DN part added")
        # Crawl through the data
        print("CRAWLER: Starting the crawl")
        crawl_ldap(base_dn, parent)
        print("CRAWLER: Finished")
        self.browser.show_all()
        self.browser.expand_to_path(self.browser.store.get_path(parent))


                                                   
