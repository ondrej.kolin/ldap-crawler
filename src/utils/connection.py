from gi.repository import GObject
import json

class Connection(GObject.GObject):
    __gtype_name__ = "connection"
    host = ""
    base_dn = ""

    def __init__(self, host, base_dn = "", bind_dn = None, password =  None):
        self.host = host
        self.base_dn = base_dn
        self.bind_dn = bind_dn
        self.password = password

        super().__init__()


    def __eq__(self, other):
        return self.host == other.host and self.base_dn == other.base_dn \
            and self.bind_dn == other.bind_dn

    def __str__(self):
        # TODO unsecured password below!
        return f'{self.bind_dn}:{self.password}@{self.host}/{self.base_dn}'

    def to_json(self):
        data = {
            "host" : self.host,
            "base_dn" : self.base_dn,
            "bind_dn" : self.bind_dn,
            "password" : self.password # admin_password TODO (pw into keyrings)?
        }
        return json.dumps(data)

    @property
    def protocol(self):
        if self.host.startswith("ldap://"):
            return "ldap://"
        elif self.host.startswith("ldaps://"):
            return "ldaps://"
        else:
            raise "Error!"

    @property
    def hostname(self):
        return self.host[len(self.protocol):]

    @property
    def port(self):
        if self.protocol.startswith("ldaps"):
            return 636
        else:
            return 389

    @staticmethod
    def from_json(data):
        data = json.loads(data)
        if "host" not in data:
            raise KeyError("Host missing from json definition")
        data.setdefault("base_dn", "")
        data.setdefault("bind_dn", None)
        data.setdefault("password", None)
        return Connection(**data)
        
