# window.py
#
# Copyright 2019 Ondrej Kolin
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, GObject
from ..utils.connection import Connection

from gi.repository import Gtk


@Gtk.Template(resource_path='/cz/ondrejkolin/ldapcrawler/dialogs/connection.ui')
class ConnectionDialog(Gtk.Dialog):
    __gtype_name__ = 'ConnectionDialog'

    protocol = Gtk.Template.Child()
    host = Gtk.Template.Child()
    base_dn = Gtk.Template.Child()

    def __init__(self, *args, **kwargs):
        kwargs["use_header_bar"] = 1
        super().__init__("New Connection", *args, **kwargs)

    def get_connection(self):
        return Connection(self.protocol.get_active_text() + self.host.get_text(), self.base_dn.get_text())


    
